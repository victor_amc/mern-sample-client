import React, {useReducer} from 'react'
import TaskContext from './TaskContext';
import taskReducer from './taskReducer';
import clientAxios from '../../config/axios'

import {TASK_GET_PROJECT,
        TASK_ADD,
        TASK_SHOW_ERROR,
        TASK_DELETE,
        TASK_UPDATE,
        TASK_SET_CURRENT
        } from '../../types';

const TaskState = props => {

    //states
    const initialState = {
        projectTasks: [],
        isShowError: false,
        currentTask: null
    }

    //Reducer: Dispatch of actions 
    const [state, dispatch] = useReducer(taskReducer, initialState);


    //CRUD functions --------------------------------
    //Get project
    const getTasks = async projectId =>{

        try{
            const response = await clientAxios.get('/api/tasks', { params: {projectId} } );

            dispatch({
                type: TASK_GET_PROJECT,
                payload: response.data.tasks
            });
        }catch(error){
            console.log(error)
        }
    }


    //Add Task
    const addTask = async task =>{
        try{
            await clientAxios.post('/api/tasks', task);

            dispatch({
                type: TASK_ADD,
                // payload: task
            });
        }catch(error){
            console.log(error)
        }
 
    }

    //show error
    const showError = () => {
        dispatch({
            type: TASK_SHOW_ERROR
        });
    }


    //delete task
    const deleteTask = async (taskId, projectId) => {

        try{
            await clientAxios.delete(`/api/tasks/${taskId}`, {params: {projectId}});

            dispatch({
                type: TASK_DELETE,
                payload: taskId
            });
        }catch(error){
            console.log(error);
        }
    }


    //update task
    const updateTask = async (task) => {
        try{
            const response = await clientAxios.put(`/api/tasks/${task._id}`, task);

            dispatch({
                type: TASK_UPDATE,
                payload: response.data
            });
        }catch(error){

        }
    }

    //set current
    const setCurrentTask = (task) => {
        dispatch({
            type: TASK_SET_CURRENT,
            payload: task
        });
    }


    //return
    return (
        <TaskContext.Provider
            value = {{
                    projectTasks: state.projectTasks,
                    isShowError: state.isShowError,
                    currentTask: state.currentTask,
                    getTasks: getTasks,
                    addTask: addTask,
                    showError: showError,
                    deleteTask: deleteTask,
                    updateTask: updateTask,
                    setCurrentTask: setCurrentTask
                }}
        >
            {props.children}
        </TaskContext.Provider>

    )
}

export default TaskState;