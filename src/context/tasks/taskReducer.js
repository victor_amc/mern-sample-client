import {TASK_GET_PROJECT,
        TASK_ADD,
        TASK_SHOW_ERROR,
        TASK_DELETE,
        TASK_UPDATE,
        TASK_SET_CURRENT
        } from '../../types';

const taskReducer = (state, action) => {
    switch(action.type)
    {
        case TASK_ADD:
            // return {...state, projectTasks: [action.payload, ...state.projectTasks], isShowError: false};
            return {...state, isShowError: false};
        case TASK_GET_PROJECT:
            return {...state, projectTasks: action.payload};
        case TASK_SHOW_ERROR :
            return {...state, isShowError: true};
        case TASK_DELETE:
            return {...state, projectTasks: state.projectTasks.filter(task => task._id !== action.payload)};
        case TASK_UPDATE:
            return {...state, projectTasks: state.projectTasks.map(task => task._id === action.payload._id ? action.payload : task)};
        case TASK_SET_CURRENT:
            return {...state, currentTask: action.payload};
        default: 
            return state;
    }
}

export default taskReducer;