import React, {useReducer} from 'react';
import ProjectContext from "./ProjectContext";
import projectReducer from "./projectReducer";
// import {v4 as uuidv4} from 'uuid';
import clientAxios from '../../config/axios';


import {PROJECT_SHOW_FORM,
        PROJECT_GET_PROJECTS,
        PROJECT_ADD,
        PROJECT_SHOW_ERROR,
        PROJECT_SET_CURRENT, 
        PROJECT_DELETE, 
        PROJECT_ERROR
    } from '../../types'


function ProjectState(props) {

    //state
    const initialState = {
        projects: [],
        currentProject: null,
        isShowForm: false,
        isShowError: false,
        message: null
    };

    //Reducer: Dispatch of actions 
    const [state, dispatch] = useReducer(projectReducer, initialState)


    //CRUD functions --------------------------------
    
    //Show form
    const showForm = () => {
        dispatch({
            type: PROJECT_SHOW_FORM
        });
    }

    //Get projects
    const getProjects = async () =>{
        try{
            const response = await clientAxios.get('/api/projects')

            dispatch({
                type: PROJECT_GET_PROJECTS,
                payload: response.data.projects
            });
        }catch(error){
            const warning = {
                msg: 'Unexepected Error',
                category: 'alert-error'
            }
            
            dispatch({
                type: PROJECT_ERROR,
                payload: warning
            })
        }
    }

    //Add Project
    const addProject = async (project) =>{
        // project.id = uuidv4(); //covered by mongoDB

        try{
            const response = await clientAxios.post('/api/projects', project);

            dispatch({
                type: PROJECT_ADD,
                payload: response.data
            });
        }catch(error){
            const warning = {
                msg: 'Unexepected Error',
                category: 'alert-error'
            }
            
            dispatch({
                type: PROJECT_ERROR,
                payload: warning
            })
        }
    }

    

    //Show new project error
    const showError = () =>{
        dispatch({
            type: PROJECT_SHOW_ERROR
        });
    }


    //Update current Project
    const setCurrentProject = (projectId) =>{
        dispatch({
            type: PROJECT_SET_CURRENT,
            payload: projectId
        });
    }

    //Delete current Project
    const deleteProject = async (projectId) =>{

        try{
            await clientAxios.delete(`/api/projects/${projectId}`); 
            dispatch({
                type: PROJECT_DELETE,
                payload: projectId
            });
        }
        catch(error){
            const warning = {
                msg: 'Unexepected Error',
                category: 'alert-error'
            }

            dispatch({
                type: PROJECT_ERROR,
                payload: warning
            })
        }
    }


    return (
        <ProjectContext.Provider
            value = {{
                projects: state.projects,
                currentProject: state.currentProject,
                isShowForm: state.isShowForm,
                isShowError: state.isShowError,
                message: state.message,
                showForm: showForm,
                getProjects: getProjects,
                addProject: addProject,
                showError: showError,
                setCurrentProject: setCurrentProject,
                deleteProject: deleteProject
            }}
            >
            {props.children}
        </ProjectContext.Provider>
    ); 
}

export default ProjectState

