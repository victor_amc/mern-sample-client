import {first} from '../../components/utilities/utilities.js'
import {PROJECT_SHOW_FORM, 
        PROJECT_GET_PROJECTS,
        PROJECT_ADD,
        PROJECT_SHOW_ERROR,
        PROJECT_SET_CURRENT, 
        PROJECT_DELETE, 
        PROJECT_ERROR
    } from '../../types'

const projectReducer = (state, action) => {
    switch (action.type){
        case PROJECT_SHOW_FORM:
            return {...state, isShowForm: true};
        case PROJECT_GET_PROJECTS:
            return {...state, projects: action.payload};
        case PROJECT_ADD:
            return {...state, projects: [...state.projects, action.payload], isShowForm: false, isShowError: false };
        case PROJECT_SHOW_ERROR:
            return {...state, isShowError: true };
        case PROJECT_SET_CURRENT:
            return {...state, currentProject: first(state.projects.filter( p => action.payload === p._id))};
        case PROJECT_DELETE:
            return {...state, projects: state.projects.filter( p => action.payload !== p._id), currentProject: null};
        case PROJECT_ERROR:
            return {...state, message: action.payload};
        default:
            return state;
    }
}

export default projectReducer;