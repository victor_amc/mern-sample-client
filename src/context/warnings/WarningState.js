import React, {useReducer} from 'react';
import warningReducer from './warningReducer';
import WarningContext from './WarningContext';

import {WARNING_SHOW, WARNING_HIDE} from '../../types';

const WarningState = props => {

    const initialState = { warning: null};

    const [state, dispatch] = useReducer(warningReducer, initialState);

    //CRUD functions --------------------------------
    const showWarning = (msg, category) =>{
        dispatch({
            type: WARNING_SHOW,
            payload: {msg, category} //object literal enhancement
        })

        //Clear Warning after 5 seconds
        setTimeout(() => {
            dispatch({
                type: WARNING_HIDE
            })
        }, 5000); //Milliseconds
    }


    return (
        <WarningContext.Provider
            value = {
                {
                    warning: state.warning,
                    showWarning: showWarning
                }
            }
        >
            {props.children}
        </WarningContext.Provider>

    )
}

export default WarningState;