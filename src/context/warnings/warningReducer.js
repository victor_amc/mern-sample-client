import {WARNING_SHOW, WARNING_HIDE} from '../../types';

const warningReducer =  (state, action) => {
    switch(action.type){
        case WARNING_SHOW:
            return {warning: action.payload};
        case WARNING_HIDE:
            return {warning: null};
        default:
            return state;
        
    }
}

export default warningReducer;