import {AUTH_LOGON_SUCCESS,
        AUTH_LOGON_ERROR, 
        AUTH_GET_USER, 
        AUTH_LOGIN_SUCCESS,
        AUTH_LOGIN_ERROR,
        AUTH_LOGOUT} from '../../types';

const authReducer = (state, action) => {
    switch(action.type){

        case AUTH_LOGIN_SUCCESS:
        case AUTH_LOGON_SUCCESS:
            localStorage.setItem('token', action.payload); //F12 > Application > LocalStorage+
            return {
                ...state, 
                isAuthenticated: true, 
                isLoading: false,
                message: null};
        case AUTH_LOGIN_ERROR:
        case AUTH_LOGON_ERROR:
            localStorage.removeItem('token');
            return {
                ...state, 
                isAuthenticated: false, 
                message: action.payload,
                isLoading: false
            };
        case AUTH_GET_USER:
            return {
                ...state, 
                isAuthenticated: true, 
                user: action.payload,
                isLoading: false
            }; 
        case AUTH_LOGOUT:
            return {
                ...state, 
                token: null,
                user: null, 
                isAuthenticated: false,
                message: action.payload
            }; 
    
        default:
            return state;
        
    }
}

export default authReducer;