import React, {useReducer} from 'react';
import authReducer from './authReducer';
import AuthContext from './AuthContext';
import clientAxios from '../../config/axios';
import authToken from '../../config/token';


import {AUTH_LOGON_SUCCESS,
    AUTH_LOGON_ERROR, 
    AUTH_GET_USER, 
    AUTH_LOGIN_SUCCESS,
    AUTH_LOGIN_ERROR,
    AUTH_LOGOUT} from '../../types';

const AuthState = props => {

    const initialState = 
    { 
        token: localStorage.getItem('token'), //F12 > Application > LocalStorage+
        isAuthenticated: null,
        user: null, 
        message: null,
        isLoading: true
    } ;

    const [state, dispatch] = useReducer(authReducer, initialState);

    //CRUD functions --------------------------------

    //Log On
    const logOn = async (data) =>{
        try{
            const response = await clientAxios.post('/api/users', data);

            //Log on
            dispatch({
                type: AUTH_LOGON_SUCCESS,
                payload: response.data.token
            })

            //Get User
            getUserAuthenticated();
        }
        catch(error){
            // console.log(error.response);

            const warning = {
                msg: (typeof error.response  === 'undefined') ? "Unexpected Error" : error.response.data.msg ,
                category: 'alert-error'
            };
            
            dispatch({
                type: AUTH_LOGON_ERROR,
                payload: warning
            })
        }
    }

    //Get user
    const getUserAuthenticated = async () => {
        const token = localStorage.getItem('token');
        if (token){
            //Send Token though headers
            authToken(token);
        }

        try{
            const response = await clientAxios.get('/api/auth');
            dispatch({
                type: AUTH_GET_USER,
                payload: response.data.user
            });

        }
        catch (error){
            // console.log(error.response);
            const warning = {
                msg: (typeof error.response  === 'undefined') ? "Unexpected Error" : error.response.data.msg ,
                category: 'alert-error'
            };
            dispatch({
                type: AUTH_LOGIN_ERROR,
                payload: warning
            })
        }
    }

    //Log In User
    const logIn = async data => {
        try{
            const response = await clientAxios.post('/api/auth', data);
            //log in
            dispatch({
                type: AUTH_LOGIN_SUCCESS,
                payload: response.data.token
            })

            //Get User
            getUserAuthenticated();
        }
        catch (error){
            // console.log(error.response);

            const warning = {
                msg: (typeof error.response  === 'undefined') ? "Unexpected Error" : error.response.data.msg ,
                category: 'alert-error'
            };
            dispatch({
                type: AUTH_LOGIN_ERROR,
                payload: warning
            })
        }
    }

    //Log Out
    const logOut = () => {
        localStorage.removeItem('token');
        dispatch({
            type: AUTH_LOGOUT
        })
    }


    return (
        <AuthContext.Provider
            value = {
                {
                    token: state.token,
                    isAuthenticated: state.isAuthenticated,
                    user: state.user,
                    message: state.message,
                    isLoading: state.isLoading,
                    logOn: logOn,
                    logIn: logIn,
                    logOut: logOut,
                    getUserAuthenticated: getUserAuthenticated
                }
            }
        >
            {props.children}
        </AuthContext.Provider>

    )
}

export default AuthState;