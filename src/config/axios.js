import axios from 'axios';

const clientAxios = axios.create({
    baseURL : process.env.REACT_APP_BACKEND_URL,
    withCredentials: false,
    headers: {
        'Access-Control-Allow-Origin' : '*',
        'Access-Control-Allow-Methods':'GET,PUT,POST,DELETE,PATCH,OPTIONS',   
        'Access-Control-Allow-Headers': '*',
    }
});

//Network error:
//https://stackoverflow.com/questions/56297420/getting-network-error-when-calling-axios-get-request-in-react-js


export default clientAxios;