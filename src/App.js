import './index.css'
import LogIn from './components/auth/LogIn'
import LogOn from './components/auth/LogOn'
import MainPage from './components/projects/MainPage'
import {BrowserRouter as Router, Switch, Route} from 'react-router-dom';

import ProjectState from './context/projects/ProjectState';
import TaskState from './context/tasks/TaskState';
import WarningState from './context/warnings/WarningState';
import AuthState from './context/auth/AuthState';

import tokenAuth from './config/token';
import PrivateRoute from './components/routes/PrivateRoute';


//If user in local storage send it to header
const token = localStorage.getItem('token');
if (token){
  tokenAuth(token);
}

function App() {

  return (
    <ProjectState>
      <TaskState>
        <WarningState>
          <AuthState>
            <Router>
              <Switch>
                <Route exact path= "/" component = {LogIn}/>
                <Route exact path= "/logon" component = {LogOn}/>
                <PrivateRoute exact path= "/main" component = {MainPage}/>
              </Switch>
            </Router>
          </AuthState>
        </WarningState>
      </TaskState>
    </ProjectState>

  );
}

export default App;
