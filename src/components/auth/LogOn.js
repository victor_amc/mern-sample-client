import React, {useState, useContext, useEffect, useRef} from 'react';
import {Link} from 'react-router-dom';
import WarningContext from '../../context/warnings/WarningContext';
import AuthContext from '../../context/auth/AuthContext';

function LogOn(props) {

    //get Context values
    const {warning, showWarning}  = useContext(WarningContext);
    const {message, isAuthenticated, logOn}  = useContext(AuthContext);

    //states
    const [user, setUser] = useState({
            name: "",
            surname: "",
            email: "",
            password: "",
            confirm:""
        });

    // destruct user
    const { name, surname, email, password, confirm } = user;

    //error in useEffect
    //https://dev.to/webcoderkz/dealing-with-infinite-loops-in-useeffect-hook-j11
    //https://youtu.be/gnkrDse9QKc?t=1919
    const showWarningRef = useRef(() => {});
    showWarningRef.current = (message) => {
        if (message)
        { 
            showWarning(message.msg, message.category);
            setUser( u => {return {...u,  password: "", confirm: ""}});
        }
    };

    //In case an error on Log On
    useEffect(() => {
        if (isAuthenticated){
            props.history.push('/main'); // React-Router-Dom
        }
        showWarningRef.current(message);
        // eslint-disable-next-line react-hooks/exhaustive-deps

    },[message, isAuthenticated, props.history, showWarningRef]); 


    
    //events
    const onChangeLogOn = (e) => {
        setUser({ ...user, [e.target.name] : e.target.value });
    }

    const onSubmit = (e) =>
    {
        e.preventDefault();

        //Validate empty string
        if (name.trim() === '' 
            || surname.trim() === ''
            || email.trim() === ''
            || password.trim() === '' 
            || confirm.trim() === '' )
        {
            showWarning('All fields are required.', 'alert-error');
            return;
        }
        
        //Server / routes : password min 6 characters
        if (password.length < 6){
            showWarning('Password must be at least 6 characters.', 'alert-error');
            return;
        }

        //Both Passwords are the same 
        if (password !== confirm){
            showWarning('Passwords are different', 'alert-error');
            return;
        }

        //action
        logOn({name, surname, email, password });
    }

    return (
        <div className= "form-user">
            {warning ? (<div className={`alert ${warning.category}`}> {warning.msg} </div>) : null}
            <div className = "form-container shadow-dark">
                <h1> Register </h1>
                
                <form
                    onSubmit = {onSubmit}
                    >
                    {/* name */}
                    <div className= "form-field">
                        <label htmlFor="name">Name</label>
                        <input 
                            type = "text"
                            id = "name"
                            name = "name"
                            placeholder ="John"
                            value={name}
                            onChange = {onChangeLogOn}
                            />
                    </div>
                    
                    {/* surname */}
                    <div className= "form-field">
                        <label htmlFor="surname">Surname</label>
                        <input 
                            type = "text"
                            id = "surname"
                            name = "surname"
                            placeholder ="White"
                            value={surname}
                            onChange = {onChangeLogOn}
                            />
                    </div>

                    {/* email */}
                    <div className= "form-field">
                        <label htmlFor="email">Email</label>
                        <input 
                            type = "email"
                            id = "email"
                            name = "email"
                            placeholder ="youremail@sample.com"
                            value={email}
                            onChange = {onChangeLogOn}
                            />
                    </div>

                    {/* pwd */}
                    <div className= "form-field">
                        <label htmlFor="password">Password</label>
                        <input 
                            type = "password"
                            id = "password"
                            name = "password"
                            placeholder =""
                            value = {password}
                            onChange = {onChangeLogOn}
                            />
                    </div>

                    {/* confirm pwd */}
                    <div className= "form-field">
                        <label htmlFor="confirm">Confirm Password</label>
                        <input 
                            type = "password"
                            id = "confirm"
                            name = "confirm"
                            placeholder = ""
                            value = {confirm}
                            onChange = {onChangeLogOn}
                            />
                    </div>

                    <div className= "form-field">
                        <input 
                            type="submit" 
                            className="btn btn-primary btn-block"
                            value="Register"
                            />
                    </div>
                </form>

                <Link to={'/'} className="account-link">
                    Back to Log In
                </Link>

            </div>
        </div>
    )
}




export default LogOn;


