import React, {useState, useContext, useEffect, useRef} from 'react';

import {Link} from 'react-router-dom';
import WarningContext from '../../context/warnings/WarningContext';
import AuthContext from '../../context/auth/AuthContext';

function LogIn(props) {

    //get Context values
    const {warning, showWarning}  = useContext(WarningContext);
    const {message, isAuthenticated, logIn}  = useContext(AuthContext);

    //states
    const [user, setUser] = useState({
            email: "",
            password: ""
        });

    const {email, password} = user;

    //error in useEffect
    //https://youtu.be/gnkrDse9QKc?t=1919
    const showWarningRef = useRef(() => {});
    showWarningRef.current = (message) => {
        if (message)
        { 
            showWarning(message.msg, message.category);
            setUser( u => {return {...u,  password: "", confirm: ""}});
        }
    };

    //In case an error on Log On
    useEffect(() => {
        if (isAuthenticated){
            props.history.push('/main'); // React-Router-Dom
        }
        showWarningRef.current(message);

    },[message, isAuthenticated, props.history, showWarningRef]); 
    
    
    
    //events
    const onChangeLogIn = (e) => {
        setUser({...user, [e.target.name] : e.target.value});
    }

    const onSubmit = (e) =>
    {
        e.preventDefault();

        //Validate empty string
        if (email.trim() === ''
            || password.trim() === '')
        {
            showWarning('All fields are required.', 'alert-error');
            return;
        }

        //action
        logIn({email, password });
    }

    return (
        <div className= "form-user">
            {warning ? (<div className={`alert ${warning.category}`}> {warning.msg} </div>) : null}
            <div className = "form-container shadow-dark">
                <h1> Log In </h1>
                
                <form
                    onSubmit = {onSubmit}
                    >

                    {/* email */}
                    <div className= "form-field">
                        <label htmlFor="email">Email</label>
                        <input 
                            type = "email"
                            id = "email"
                            name = "email"
                            placeholder ="youremail@sample.com"
                            value={email}
                            onChange = {onChangeLogIn}
                            />
                    </div>

                    {/* pwd */}
                    <div className= "form-field">
                        <label htmlFor="password">Password</label>
                        <input 
                            type = "password"
                            id = "password"
                            name = "password"
                            placeholder =""
                            value = {password}
                            onChange = {onChangeLogIn}
                            />
                    </div>

                    <div className= "form-field">
                        <input 
                            type="submit" 
                            className="btn btn-primary btn-block"
                            value="Log In"
                            />
                    </div>
                </form>

                <Link to={'/logon'} className="account-link">
                    Hey, are you new? Why not Register an account with us, it is free.
                </Link>

            </div>
        </div>
    );
}


export default LogIn;
