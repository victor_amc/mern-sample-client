import React, {useContext, useEffect, useRef} from 'react';
import Sidebar from '../layout/Sidebar';
import Header from '../layout/Header';
import TaskEditor from '../tasks/TaskEditor';
import Tasks from '../tasks/Tasks';

import AuthContext from '../../context/auth/AuthContext'


function MainPage(props) {

    //Get auth data
    const {getUserAuthenticated} = useContext(AuthContext);

    //Authtenticate
    const getUserAuthenticatedRef = useRef(() => {});
    getUserAuthenticatedRef.current = () => {
        getUserAuthenticated();
    };

    //Update
    useEffect(() => {
        getUserAuthenticatedRef.current();
    }, []);


    return (
        <div className="container-app">
            <Sidebar/>
            
            <div className = "main">
                <Header/>

                <main>

                    <TaskEditor/>

                    <div className="tasks-container">
                        <Tasks/>
                    </div>
                    
                </main>
            </div>
        </div>
    )
}

export default MainPage

