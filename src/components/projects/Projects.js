import React, {useEffect, useContext} from 'react'
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import Project from './Project'
import ProjectContext from '../../context/projects/ProjectContext';
import WarningContext from '../../context/warnings/WarningContext';


function Projects(props) {

    //initial state
    const pContext = useContext(ProjectContext);
    const {message, projects, getProjects} = pContext;

    const wContext = useContext(WarningContext);
    const {warning, showWarning} = wContext;
        
    //update
    useEffect(() => {

        //If error
        if(message){
            showWarning(message.msg, message.category);
        }

        getProjects();
        //eslint-disable-next-line
    }, [message])

    //https://reactjs.org/docs/strict-mode.html#warning-about-deprecated-finddomnode-usage
    //node (for transition?)
    const nodeRef = React.useRef(null);

    //if empty
    if (projects.length === 0) return (<p>Start by adding a project.</p>);

    return (
        <ul className="projects-list">

            {warning ? <div className={`alert ${warning.category}`}>{warning.msg}</div> : null} 

            <TransitionGroup>
                {projects.map(project=>(
                    <CSSTransition 
                        nodeRef = {nodeRef}
                        key = {project._id}
                        timeout = {200}
                        classNames= "project">
                        <Project  project = {project} />
                    </CSSTransition>
                ))}
            </TransitionGroup> 

        </ul>
    )
}

export default Projects

