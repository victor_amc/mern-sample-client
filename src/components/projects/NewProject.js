import React, {Fragment, useState, useContext} from 'react';
import projectContext from '../../context/projects/ProjectContext';

function NewProject(props) {

    //context
    const pContext = useContext(projectContext);
    const {isShowForm, isShowError, showForm, addProject, showError} = pContext;

    //states
    const [project, setProject] = useState({
        name:''
    })

    //events
    const onChangeProject = (e) =>{
        setProject({...project, [e.target.name]:e.target.value});
    };

    const onSubmit = (e) =>{
        e.preventDefault();

        //validate
        if (project.name === '')
        {
            showError();
            return;
        }

        //add to state
        addProject(project);

        //reset form
        setProject({ name:''});
    };

    return (
        <Fragment>
        <button
            type="button"
            className="btn btn-block btn-primary"
            onClick = {() => showForm()}
            >
            New Project
        </button>

        {
            isShowForm 
            ?
            (
                <form 
                    className="form-new-project"
                    onSubmit = {onSubmit}  
                        >
                    <input
                        type="text"
                        className="input-text"
                        placeholder="Project Name"
                        name="name"
                        value={project.name}
                        onChange= {onChangeProject}
                        />
        
                    <input 
                        type="submit"
                        className="btn btn-primary btn-block"
                    />
                    
                </form>
            )
            : null
            }

            {isShowError ? <p className="message error">Name is Required</p>: null}
            
        </Fragment>
    )
}

export default NewProject

