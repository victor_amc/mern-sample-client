import React, {useContext} from 'react';
import ProjectContext from '../../context/projects/ProjectContext';
import TaskContext from '../../context/tasks/TaskContext';

function Project({project}) {

    //context
    const pContext = useContext(ProjectContext);
    const {setCurrentProject} = pContext;

    const tContext = useContext(TaskContext);
    const {getTasks} = tContext;

    //events
    const onClick  = () => {
        setCurrentProject(project._id);
        getTasks(project._id);
    }   

    return (
        <li>
            <button
                type ="button"
                className="btn btn-blank"
                onClick = {onClick}
                >
                {project.name}
            </button>
        </li>
    )
}

export default Project

