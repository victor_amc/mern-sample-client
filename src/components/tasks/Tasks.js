import React, {Fragment, useContext} from 'react';
import {CSSTransition, TransitionGroup} from 'react-transition-group';
import ProjectContext from '../../context/projects/ProjectContext';
import TaskContext from '../../context/tasks/TaskContext';
import Task from './Task';

function Tasks(props) {

    //context
    const pContext = useContext(ProjectContext);
    const {currentProject, deleteProject} = pContext;

    const tContext = useContext(TaskContext);
    const {projectTasks} = tContext;

    //https://reactjs.org/docs/strict-mode.html#warning-about-deprecated-finddomnode-usage
    //node (for transition?)
    const nodeRef = React.useRef(null);

    //If Null or Undefined
    if (currentProject === null || typeof currentProject === 'undefined') return (<h2> Select a Project</h2>);

    return (
        <Fragment>
        <h2>Project: {currentProject.name}  </h2>

            {/* Tasks */}
            <ul className = "tasks-list">
                {
                    projectTasks.length === 0 ? 
                    (<li className = "task"><p>Yo, No tasks.</p></li>)
                     :
                     <TransitionGroup>
                         {
                            projectTasks.map(task =>(
                                <CSSTransition
                                    key = {task._id}
                                    nodeRef = {nodeRef}
                                    timeout = {200}
                                    classNames= "task"
                                    >
                                    <Task task = {task} />
                                </CSSTransition>
                                ))}
                     </TransitionGroup> 
                }
            </ul>  

            {/* Delete Button */}
            < button 
                    type = "button"
                    className = "btn btn-delete"
                    onClick = {() => deleteProject(currentProject._id)}
                >
                Delete Project &times;
            </button>

        </Fragment>

    )
}

export default Tasks

