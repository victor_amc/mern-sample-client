import {React, useContext} from 'react'
import ProjectContext from '../../context/projects/ProjectContext';
import TaskContext from '../../context/tasks/TaskContext';

function Task({task}) {

    //context
    const {currentProject} = useContext(ProjectContext)
    const {deleteTask, getTasks, updateTask, setCurrentTask} = useContext(TaskContext);

    //events
    const onClickDelete = () =>
    {
        deleteTask(task._id, currentProject._id);
        getTasks(currentProject._id);
    }

    const onClickState = () =>
    {
        //toggle status
        task.status = !task.status;
        //update status
        updateTask(task);
    }

    const onClickEdit = () =>
    {
        //set current 
        setCurrentTask(task);
    }

    return (
        <li className = "task shadow"> 
            <p>{task.name} </p> 
            <div className="status">
                {
                    task.status ?
                    (<button 
                        type = "button"
                        className ="complete"
                        onClick = {onClickState}
                        >
                        Completed
                    </button>)
                    :
                    (<button 
                        type = "button"
                        className ="incomplete"
                        onClick = {onClickState}
                        >
                        Incomplete
                    </button>)
                }
            </div>

            <div className="actions">
                <button 
                    type ="button" 
                    className="btn btn-primary"
                    onClick = {onClickEdit}
                >
                    Edit
                </button>
                <button 
                    type ="button" 
                    className="btn btn-secondary"
                    onClick = {onClickDelete}
                >
                    Delete
                </button>

            </div>
        </li>
    )
}

export default Task

