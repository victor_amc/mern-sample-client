import React, {useState, useEffect, useContext} from 'react';
import ProjectContext from '../../context/projects/ProjectContext';
import TaskContext from '../../context/tasks/TaskContext';



function TaskEditor(props) {

    const getEmtpyTask =  () => {
        return {
        name: '',
        status: false,
        projectId: '' }
    };

    //context
    const pContext = useContext(ProjectContext);
    const {currentProject} = pContext;

    const tContext = useContext(TaskContext);
    const {currentTask, addTask, isShowError, showError, getTasks, updateTask, setCurrentTask} = tContext;

    //state 
    const [task, setTask] = useState(getEmtpyTask());

    //update
    useEffect(() => {
        if (currentTask  !== null)
            setTask(currentTask)
        else
            setTask(getEmtpyTask())
        
    }, [currentTask])


    //If Null
    if (currentProject === null) return null;

    //events
    const onChange = (e) =>{   
        setTask({
            ...task, [e.target.name]: e.target.value
        })
    }


    const onSubmit = (e) => {
        e.preventDefault();

        //validate
        if (task.name.trim() === '') 
        {
            //show error
            showError();
            return;
        }

        //if is edit or add
        if (currentTask === null)
        {
            //add task
            task.projectId = currentProject._id;
            addTask(task);
        }
        else
        {
            updateTask(task);
            setCurrentTask(null);
        }

        //update tasks
        getTasks(currentProject._id)

        //reset 
        setTask(getEmtpyTask());


    }

    return (
        <div className="form">
            <form
                onSubmit = {onSubmit}
            >
                {/* textfield */}
                <div className="input-container">
                    <input 
                        type="text"
                        className="input-text"
                        placeholder="My task"
                        name="name"
                        value = {task.name}
                        onChange = {onChange}
                    />
                </div>
                
                {/* submit */}
                <div className="input-container">
                    <input 
                        type="submit"
                        className="btn btn-primary btn-block"
                        value ={currentTask ? "Edit Task": "Add Task"}
                    />
                </div>

                {isShowError ? (<p className='message alert-error'>Task name is requierd.</p>) : null }

            </form>
            
            
        </div>
    )
}

export default TaskEditor

