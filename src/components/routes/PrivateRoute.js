import React, {useContext, useEffect, useRef} from 'react';
import {Route, Redirect} from 'react-router-dom';
import AuthContext from '../../context/auth/AuthContext';

//Higher order component
const PrivateRoute = ({component: Component, ...props}) => {
    
    //Get auth data
    const {isAuthenticated, getUserAuthenticated} = useContext(AuthContext);

    //Authtenticate
    const getUserAuthenticatedRef = useRef(() => {});
    getUserAuthenticatedRef.current = () => {
        getUserAuthenticated();
    };

    //Update
    useEffect(() => {
        getUserAuthenticatedRef.current();
    }, []);

    return (
        <Route 
            // Renders first the and then useEffect loads authentication
            {...props} render= { props => !isAuthenticated  ? ( //&& !isLoading //was used to avoid flashing effect when refreshing but is not a good effect if logout.
                    <Redirect to = "/" />
                ): (
                    <Component {...props} />
                )
            }
        />
    );
}

export default PrivateRoute;

