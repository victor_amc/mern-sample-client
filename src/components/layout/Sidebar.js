import React from 'react';
import NewProject from '../projects/NewProject'
import Projects from '../projects/Projects'

function Sidebar({projects}) {

    return (
        <aside>
            <h1>MERN <span>Tasks</span></h1>

            <NewProject/>

            {/* Title */}
            <div className= "projects">
                <h2>My Projects</h2>
            </div>

            <Projects/>

            
        </aside>
    )
}

export default Sidebar

