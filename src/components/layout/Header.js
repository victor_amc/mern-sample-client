import React, {useContext, useRef, useEffect} from 'react';
import AuthContext from '../../context/auth/AuthContext';

function Header() {

    //Get auth data
    const {user, getUserAuthenticated, logOut} = useContext(AuthContext);

    //Authtenticate
    const getUserAuthenticatedRef = useRef(() => {});
    getUserAuthenticatedRef.current = () =>  getUserAuthenticated();
    

    //Update
    useEffect(() => {
        getUserAuthenticatedRef.current();
    }, []);

    return (
        <header className = "app-header">
            {user ? <p className = "username">How is it going: <span>{user.name}</span></p> : null }

            <nav className="nav-principal">
                {/* Syntax for empty link <a href="#!">Log Out</a> */}
                <button 
                    className = "btn btn-blank log-out"
                    onClick = {() => logOut()}
                >
                    Log Out
                </button>
            </nav>

        </header>
    )
}

export default Header

